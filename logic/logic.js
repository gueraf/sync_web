const moodle_token = "7008ccd79805d4f9a40ab808123f1953";

var async = require("async");
var und = require('underscore');

var moodle = require("sync_moodle");
var mongoose = require("sync_mongo");

module.exports = function(routes) {
  /* initialize database connection */
  mongoose.connect();

  /* add routes for db access */
  /* get subscriptions of auth. user */
  routes.get("/json/db", isAuthenticated, function(req, res) {
    getCoursesFromMoodleAndDB(moodle_token, req.user.email, function(err, courses) {
      if (err) {
        console.log(err);
        responseErrorJSON(res);
      } else {
        res.writeHead(200, {
          "Content-Type": "application/json"
        });
        res.end(JSON.stringify(courses));
      }
    });
  });

  /* set subscriptions of auth. user (create if non-existent) */
  routes.post("/json/db", isAuthenticated, function(req, res) {
    getCoursesFromMoodleAndDB(moodle_token, req.user.email, function(err, courses) {
      if (err) {
        console.log(err);
        respondWithCode(res, 500);
      } else {
        var user_selections;
        try {
          user_selections = req.body.selection ? JSON.parse(req.body.selection) : [];
        } catch (err) {
          console.log(err);
          respondWithCode(res, 400);
        }

        var allowed_selections = und.pluck(courses, "id");

        // Check if user has sent invalid course IDs: some not in allowed
        if (user_selections.some(function(course) {
            return !und.contains(allowed_selections, course);
          })) {
          console.log("User " + req.user.email + " tried to inject course ids");
          respondWithCode(res, 400);
        } else { // all IDs provided by user are OK
          mongoose.getUser(req.user.email, function(err, user) {
            if (err) {
              console.log(err);
              respondWithCode(res, 500);
            } else {
              user.displayName = req.user.displayName;
              user.dropboxToken = req.user.dropboxToken.accessToken;
              user.subscriptionCourseIds = user_selections;
              user.save(function(err) {
                if (err) {
                  console.log(err);
                  respondWithCode(res, 500);
                } else {
                  respondWithCode(res, 200);
                }
              });
            }
          });
        }
      }
    });
  });
};

/*
 * TODO: Remove unused fields.
 * Get course ids of a user from moodle and mongo. The functions returns all
 * course ids returned by moodle and it annotates whether the course has already
 * been subscribed to before.
 */
var getCoursesFromMoodleAndDB = function(token, email, callback) {
  async.parallel({
      moodle: function(callback) {
        moodle.getUserCourseIDsByEmail(token, email, callback);
      },
      mongoose: function(callback) {
        mongoose.getUser(email, callback);
      }
    },
    function(err, data) {
      if (err) {
        callback(err);
      } else {
        moodle.getCourses(moodle_token, data.moodle, function(err, courses) {
          if (err) {
            callback(err);
          } else {
            courses.forEach(function(course) {
              course.selected = und.contains(data.mongoose.subscriptionCourseIds, course.id);
            });
            callback(null, courses);
          }
        });
      }
    });
};

var isAuthenticated = function(req, res, next) {
  if (req.isAuthenticated()) {
    return next();
  }
  responseErrorJSON(res);
};


var responseErrorJSON = function(res) {
  res.writeHead(403, {
    "Content-Type": "application/json"
  });
  res.end("{\"error\": true}");
};

var respondWithCode = function(res, code) {
  res.writeHead(code);
  res.end();
};

/*
 * TODO: Handle deleted courses
 */

/*
 * TODO!
 * Get course ids of a user from moodle and mongo. The functions returns all
 * course ids returned by moodle and it annotates whether the course has already
 * been subscribed to before.
 */
/*
exports.getUserCourses = function(token, email, callback) {
  async.parallel({
      moodle: function(callback) {
        moodle.getUserCourseIDsByEmail(token, email, callback);
      },
      mongoose: function(callback) {
        mongoose.getUser(email, callback);
      }
    },
    function(err, data) {
      if (err) {
        callback(err);
      } else {
        moodle.getCourses(token, data.moodle, function(err, courses) {
          if (err) {
            callback(err);
          } else {
            courses.forEach(function(course) {
              course.prev_checked = und.contains(data.mongoose
                .courseids, course.id);
            });
            callback(null, {
              courses: courses,
              user: data.mongoose
            });
          }
        });
      }
    });
};
*/

/*
 * Make sure that no invalid courses can be injected!
 */
exports.setUserCourses = function(user, allowed_courses, selected_courses,
  callback) {
  // Somebody might inject courses that he is not enrolled in.
  // TODO: Move this check closer to frontend?
  var is_subset = selected_courses.every(function(course) {
    return allowed_courses.indexOf(course) >= 0;
  });
  if (!is_subset) {
    callback(new Error("Selected courses is not subset of allowed courses: " +
      selected_courses + " -- " + allowed_courses));
  } else {
    user.courseids = selected_courses;
    mongoose.setUser(user, callback);
  }
}
