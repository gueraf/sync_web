const DROPBOX_CLIENT_ID = "n1hgoph5ckpvlzz";
const DROPBOX_CLIENT_SECRET = "ly4g6vl9mi1a97c";

module.exports = function(passport, routes) {
  /* Add dropbox authentication to passport */
  var DropboxOAuth2Strategy = require("passport-dropbox-oauth2").Strategy;
  passport.use(new DropboxOAuth2Strategy({
      clientID: DROPBOX_CLIENT_ID,
      clientSecret: DROPBOX_CLIENT_SECRET,
      callbackURL: "http://localhost:3000/auth/dropbox/callback"
    },
    function(accessToken, refreshToken, profile, done) {
      var user = {
        id: profile.id,
        displayName: profile.displayName,
        email: profile.emails[0].value,
        dropboxToken: {
          'accessToken': accessToken,
          'refreshToken': refreshToken
        }
      };
      return done(null, user);
    }
  ));

  passport.serializeUser(function(user, done) {
    done(null, user);
  });

  passport.deserializeUser(function(obj, done) {
    done(null, obj);
  });

  /* add routes for dropbox authentication */
  /* GET dropbox oauth */
  routes.get("/auth/dropbox", passport.authenticate("dropbox-oauth2", {
    failureRedirect: "/",
    failureFlash: "Dropbox sign-in failed",
    successFlash: "Dropbox sign-in successful"
  }, function(req, res) {
    res.redirect("/user");
  }));

  /* GET dropbox oauth callback */
  routes.get("/auth/dropbox/callback", passport.authenticate("dropbox-oauth2", {
    failureRedirect: "/user",
    failureFlash: "Dropbox sign-in failed.",
    successFlash: "Dropbox sign-in successful."
  }), function(req, res) {
    res.redirect("/user");
  });
};
