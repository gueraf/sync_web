/*
var ldapStrategy = require('passport-ldapauth');
passport.use(new ldapStrategy({
  server: {
    url: "ldaps://iauth.tum.de:636",
    bindDn: "cn=in-wwwsrv1,ou=bindDNs,ou=iauth,dc=tum,dc=de", //"cn=in-wwwsrv1,ou=svc-users,ou=auth,ou=integratum,dc=tum,dc=de",
    bindCredentials: "geh-heim",
    searchBase: 'ou=passport-ldapauth',
    searchFilter: '(imEmailAdressen={{username}})'
  }
}));
*/

/*
var shibbolethStrategy = require("passport-shibboleth").Strategy;
passport.use(new shibbolethStrategy({
  entryPoint: "https://test-idp.ukfederation.org.uk/idp/profile/SAML2/Redirect/SSO",
  authUrl: "https://test-idp.ukfederation.org.uk/idp/shibboleth",
}, function(profile, done) {
  findByEmail(profile.mail, function(err, user) {
    if (err) {
      return done(err);
    } else {
      return done(null, user);
    }
  })
}));
*/

/*
var samlStrategy = require("passport-saml").Strategy;
passport.use(new samlStrategy({
  path: "/login/callback",
  entryPoint: "https://tumidp.lrz.de/idp/profile/SAML2/Redirect/SSO",
  //"https://tumidp.lrz.de/idp/shibboleth",
  //
  issuer: "https://www.moodle.tum.de/auth/shibboleth/index.php",
  //cert: null,
  //privateCert: null,
  //decryptionPvk: null,
  identifierFormat: "urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST", //"urn:oasis:names:tc:SAML:1.1:nameid-format:emailAddress"
  //https://www.moodle.tum.de/auth/shibboleth/index.php
}, function(profile, done) {
  findByEmail(profile.email, function(err, user) {
    if (err) {
      return done(err);
    }
    return done(null, user);
  })
}));
*/

/* GET login */
router.post('/login', passport.authenticate('ldapauth', {
  failureRedirect: '/',
  failureFlash: true,
  failureFlash: "Invalid username or password.",
  successFlash: "Welcome!"
}), function(req, res, next) {
  res.redirect('/');
});

router.post('/login/callback', passport.authenticate('ldapauth', {
  failureRedirect: '/',
  failureFlash: "Invalid username or password.",
  successFlash: "Welcome!"
}), function(req, res) {
  res.redirect('/');
});
