module.exports = function(passport, routes) {
  /* Add local authentication to passport */
  var LocalStrategy = require('passport-local').Strategy;
  passport.use(new LocalStrategy(function(username, password, done) {
    // asynchronous verification, for effect...
    process.nextTick(function() {
      // Find the user by username.  If there is no user with the given
      // username, or the password is not correct, set the user to `false` to
      // indicate failure and set a flash message.  Otherwise, return the
      // authenticated `user`.
      findByUsername(username, function(err, user) {
        console.log(user);
        if (err) {
          return done(err);
        }
        if (!user) {
          return done(null, false, {
            message: 'Unknown user ' + username
          });
        }
        if (user.password != password) {
          return done(null, false, {
            message: 'Invalid password'
          });
        }
        return done(null, user);
      })
    });
  }));

  const users = [{
    id: 1,
    username: 'bob',
    password: 'secret',
    email: 'bob@example.com'
  }, {
    id: 2,
    username: 'joe',
    password: 'birthday',
    email: 'joe@example.com'
  }];

  var findById = function(id, fn) {
    var idx = id - 1;
    if (users[idx]) {
      fn(null, users[idx]);
    } else {
      fn(new Error('User ' + id + ' does not exist'));
    }
  };

  var findByUsername = function(username, fn) {
    for (var i = 0, len = users.length; i < len; i++) {
      var user = users[i];
      if (user.username === username) {
        return fn(null, user);
      }
    }
    return fn(null, null);
  };

  passport.serializeUser(function(user, done) {
    done(null, user.id);
  });

  passport.deserializeUser(function(id, done) {
    findById(id, function(err, user) {
      done(err, user);
    });
  });

  /* add routes for local authentication */
  /* POST fake_login */
  routes.post("/local_login", passport.authenticate('local', {
    successRedirect: '/user',
    failureRedirect: '/',
    failureFlash: true
  }));
};
