module.exports = function(express) {
  var router = express.Router();

  var isAuthenticated = function(req, res, next) {
    if (req.isAuthenticated()) {
      return next();
    }
    res.redirect("/");
  }

  /* GET home page. */
  router.get("/", function(req, res, next) {
    res.render("index");
  });

  /* GET users area. */
  router.get("/user", isAuthenticated, function(req, res, next) {
    console.log(req.user);
    res.render("user", {
      username: req.user.displayName,
      email: req.user.email
    });
  });

  /* GET logout */
  router.get("/logout", function(req, res) {
    req.logout({
      successFlash: "Bye!"
    });
    res.redirect("/");
  });

  return router;
}
